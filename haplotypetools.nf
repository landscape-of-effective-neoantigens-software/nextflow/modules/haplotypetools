process haplotypetools {

  tag "${dataset}/${pat_name}/${norm_run}_${tumor_run}"
  label "haplotypetools_container"
  label "haplotypetools"

  input:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path(vcf), path(csi), path(bam), path(bai)
  tuple path(ref), path(idx)
  val parstr

  output:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path("*phased.vcf"), emit: phased_vcfs

  script:
  """
  AVCF=`echo ${vcf}`

  perl /HaplotypeTools/HaplotypeTools.pl \
  ${parstr} \
  -f ${ref} \
  -v ${vcf} \
  -b ${bam} \
  -o \${AVCF%.vcf*}.phased.vcf \
  -y \${AVCF%.vcf*}.phased.vcf.summary
  """
}


process haplotypetools_tumor {

  tag "${dataset}/${pat_name}/${norm_run}_${tumor_run}"
  label "haplotypetools_container"
  label "haplotypetools"

  input:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(rna_run), val(dataset), path(vcf), path(csi), path(dna_bam), path(dna_bai), path(rna_bam), path(rna_bai)
  tuple path(ref), path(idx)
  val parstr

  output:
  tuple val(pat_name), val(norm_run), val(tumor_run), val(dataset), path("*phased.vcf"), emit: phased_vcfs

  script:
  """
  AVCF=`echo ${vcf}`

  perl /HaplotypeTools/HaplotypeTools.pl \
  ${parstr} \
  -f ${ref} \
  -v ${vcf} \
  -b ${dna_bam},${rna_bam} \
  -o \${AVCF%.vcf*}.phased.vcf \
  -y \${AVCF%.vcf*}.phased.vcf.summary
  """
}
